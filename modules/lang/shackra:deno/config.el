;;; lang/deno/config.el -*- lexical-binding: t; -*-
(use-package! deno-fmt
  :commands (deno-fmt))
