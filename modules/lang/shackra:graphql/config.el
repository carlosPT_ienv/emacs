;;; lang/shackra:graphql/config.el -*- lexical-binding: t; -*-

(use-package! graphql-mode
  :commands (graphql-mode))
